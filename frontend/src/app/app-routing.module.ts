import { ResidenciaGeralNewComponent } from './pages/residenciaGeral/residencia-geral-new/residencia-geral-new.component';
import { ResidenciaGeralReadComponent } from './pages/residenciaGeral/residencia-geral-read/residencia-geral-read.component';
import { MarmitaUpdateComponent } from './pages/marmita/marmita-update/marmita-update.component';
import { MarmitaNewComponent } from './pages/marmita/marmita-new/marmita-new.component';
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {MainComponent} from '@modules/main/main.component';
import {LoginComponent} from '@modules/login/login.component';
import {DashboardComponent} from '@pages/dashboard/dashboard.component';
import {AuthGuard} from '@guards/auth.guard';
import {NonAuthGuard} from '@guards/non-auth.guard';
import {PrivacyPolicyComponent} from '@modules/privacy-policy/privacy-policy.component';
import { MarmitaReadComponent } from '@pages/marmita/marmita-read/marmita-read.component';
import { MarmitaDeleteComponent } from '@pages/marmita/marmita-delete/marmita-delete.component';


const routes: Routes = [
    {
        path: '',
        component: MainComponent,
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        children: [
            {
                path: 'marmitas/delete/:id',
                component: MarmitaDeleteComponent
            },
            {
                path: 'marmitas/update/:id',
                component: MarmitaUpdateComponent
            },
            {
                path: 'marmitas/add',
                component: MarmitaNewComponent
            },
            {
                path: '',
                component: DashboardComponent
            },
            {
                path: 'marmitas',
                component: MarmitaReadComponent
            },
            {
                path: 'residenciasGerais',
                component: ResidenciaGeralReadComponent
            },
            {
              path: 'residenciaGeral/add',
              component: ResidenciaGeralNewComponent

            }



        ]
    },
    {
        path: 'login',
        component: LoginComponent,
        canActivate: [NonAuthGuard]
    },
    {
        path: 'privacy-policy',
        component: PrivacyPolicyComponent,
        canActivate: [NonAuthGuard]
    },
    {path: '**', redirectTo: ''}
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {relativeLinkResolution: 'legacy'})],
    exports: [RouterModule]
})
export class AppRoutingModule {}
