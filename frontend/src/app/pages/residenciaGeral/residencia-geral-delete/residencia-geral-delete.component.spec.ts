import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResidenciaGeralDeleteComponent } from './residencia-geral-delete.component';

describe('ResidenciaGeralDeleteComponent', () => {
  let component: ResidenciaGeralDeleteComponent;
  let fixture: ComponentFixture<ResidenciaGeralDeleteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResidenciaGeralDeleteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResidenciaGeralDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
