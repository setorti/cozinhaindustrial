import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResidenciaGeralNewComponent } from './residencia-geral-new.component';

describe('ResidenciaGeralNewComponent', () => {
  let component: ResidenciaGeralNewComponent;
  let fixture: ComponentFixture<ResidenciaGeralNewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResidenciaGeralNewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResidenciaGeralNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
