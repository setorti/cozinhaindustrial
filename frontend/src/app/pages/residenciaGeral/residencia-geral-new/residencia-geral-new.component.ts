import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ResidenciaGeralService } from '@services/residencia-geral.service';
import { ResidenciaGeral } from '../residenciageral.model';

@Component({
  selector: 'app-residencia-geral-new',
  templateUrl: './residencia-geral-new.component.html',
  styleUrls: ['./residencia-geral-new.component.scss']
})
export class ResidenciaGeralNewComponent implements OnInit {

  residenciaGeral: ResidenciaGeral = {
    quantidadeMisionarios: 0,
    nomeResidenciaGeral: ''
  }
constructor(private residenciageralService: ResidenciaGeralService,
                  private router: Router) { }

ngOnInit(): void {

}

createResidenciaGeral() : void {
  this.residenciageralService.create(this.residenciaGeral).subscribe(() => {
    this.router.navigate(['/residenciasGerais'])

  })

}

}
