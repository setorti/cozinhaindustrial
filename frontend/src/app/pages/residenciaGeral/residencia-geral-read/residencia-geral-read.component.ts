import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ResidenciaGeral } from './../residenciaGeral.model';
import { ResidenciaGeralService } from '@services/residencia-geral.service';

@Component({
  selector: 'app-residencia-geral-read',
  templateUrl: './residencia-geral-read.component.html',
  styleUrls: ['./residencia-geral-read.component.scss']
})
export class ResidenciaGeralReadComponent implements OnInit {

residenciasGerais: ResidenciaGeral[] = [];
  residenciaGeral: ResidenciaGeral;

  constructor(private residenciaGeralService: ResidenciaGeralService,
              private router: Router,
              ) { }

  ngOnInit(): void {
    this.residenciaGeralService.read().subscribe(residenciasGerais =>
      {this.residenciasGerais = residenciasGerais
      console.log(this.residenciasGerais)
      })

  }

  navigateToResidenciaGeralNew(): void{
    this.router.navigate(['residenciaGeral/add'])

  }



}
