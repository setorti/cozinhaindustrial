import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResidenciaGeralReadComponent } from './residencia-geral-read.component';

describe('ResidenciaGeralReadComponent', () => {
  let component: ResidenciaGeralReadComponent;
  let fixture: ComponentFixture<ResidenciaGeralReadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResidenciaGeralReadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResidenciaGeralReadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
