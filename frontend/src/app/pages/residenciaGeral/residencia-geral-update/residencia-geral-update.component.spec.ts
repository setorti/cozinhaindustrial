import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResidenciaGeralUpdateComponent } from './residencia-geral-update.component';

describe('ResidenciaGeralUpdateComponent', () => {
  let component: ResidenciaGeralUpdateComponent;
  let fixture: ComponentFixture<ResidenciaGeralUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResidenciaGeralUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResidenciaGeralUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
