import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MarmitaService } from '@services/marmita.service';
import { Marmita } from '../marmita.model';

@Component({
  selector: 'app-marmita-delete',
  templateUrl: './marmita-delete.component.html',
  styleUrls: ['./marmita-delete.component.scss']
})
export class MarmitaDeleteComponent implements OnInit {
  marmita: Marmita;
  constructor(

    private marmitaService: MarmitaService,
    private router: Router, 
    private route: ActivatedRoute


  ) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.marmitaService.readById(id).subscribe((marmita) => {
      this.marmita = marmita;
    });
  }

  deleteMarmita(): void {
    this.marmitaService.delete(this.marmita.id).subscribe(() => {
      this.router.navigate(["/marmitas"]);
    });
  }


}
