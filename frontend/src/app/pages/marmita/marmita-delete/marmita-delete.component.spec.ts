import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarmitaDeleteComponent } from './marmita-delete.component';

describe('MarmitaDeleteComponent', () => {
  let component: MarmitaDeleteComponent;
  let fixture: ComponentFixture<MarmitaDeleteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarmitaDeleteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarmitaDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
