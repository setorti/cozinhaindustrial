import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MarmitaService } from '@services/marmita.service';
import { Marmita } from '../marmita.model';

@Component({
  selector: 'app-marmita-update',
  templateUrl: './marmita-update.component.html',
  styleUrls: ['./marmita-update.component.scss']
})
export class MarmitaUpdateComponent implements OnInit {

  marmita!: Marmita;

  constructor(
    private marmitaService: MarmitaService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get("id");
    console.log(id);
    this.marmitaService.readById(id).subscribe((marmita) => {
      this.marmita = marmita;
    });
  }

  updateMarmita(): void {
    this.marmitaService.update(this.marmita).subscribe(() => {
      
      this.router.navigate(["/marmitas"]);
    });

    
  }

  cancel(): void {
    this.router.navigate(["/marmitas"]);
  }

}
