import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarmitaUpdateComponent } from './marmita-update.component';

describe('MarmitaUpdateComponent', () => {
  let component: MarmitaUpdateComponent;
  let fixture: ComponentFixture<MarmitaUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarmitaUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarmitaUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
