import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarmitaNewComponent } from './marmita-new.component';

describe('MarmitaNewComponent', () => {
  let component: MarmitaNewComponent;
  let fixture: ComponentFixture<MarmitaNewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarmitaNewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarmitaNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
