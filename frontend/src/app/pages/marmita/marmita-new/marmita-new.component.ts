import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MarmitaService } from '@services/marmita.service';
import { Marmita } from '../marmita.model';

@Component({
  selector: 'app-marmita-new',
  templateUrl: './marmita-new.component.html',
  styleUrls: ['./marmita-new.component.scss']
})
export class MarmitaNewComponent implements OnInit {

  
    marmita: Marmita = {
      nome: '',
    

    }
  constructor(private marmitaService: MarmitaService,
                    private router: Router) { }

  ngOnInit(): void {
  
  }

  createMarmita() : void {
    this.marmitaService.create(this.marmita).subscribe(() => {
      this.router.navigate(['/marmitas'])
      
    })

}
}
