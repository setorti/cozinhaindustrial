import { Component, OnInit } from '@angular/core';
import { MarmitaService } from '@services/marmita.service';
import { Marmita } from './../marmita.model';
import { Router } from '@angular/router'

@Component({
  selector: 'app-marmita-read',
  templateUrl: './marmita-read.component.html',
  styleUrls: ['./marmita-read.component.scss']
})
export class MarmitaReadComponent implements OnInit {

  marmitas: Marmita[] = []; 
  marmita: Marmita;

  constructor(private marmitaService: MarmitaService,
              private router: Router,
              ) { }

  ngOnInit(): void {
    this.marmitaService.read().subscribe(marmitas =>
      {this.marmitas = marmitas
      console.log(this.marmitas)
      })

  }

  navigateToMarmitaNew(): void{
    this.router.navigate(['marmitas/add'])

  }



}
