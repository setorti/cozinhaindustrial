import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarmitaReadComponent } from './marmita-read.component';

describe('MarmitaReadComponent', () => {
  let component: MarmitaReadComponent;
  let fixture: ComponentFixture<MarmitaReadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarmitaReadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarmitaReadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
