import { ForDirective } from './directives/for.directive';
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

import {AppRoutingModule} from '@/app-routing.module';
import {AppComponent} from './app.component';
import {MainComponent} from '@modules/main/main.component';
import {LoginComponent} from '@modules/login/login.component';
import {HeaderComponent} from '@modules/main/header/header.component';
import {FooterComponent} from '@modules/main/footer/footer.component';
import {MenuSidebarComponent} from '@modules/main/menu-sidebar/menu-sidebar.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RegisterComponent} from '@modules/register/register.component';
import {DashboardComponent} from '@pages/dashboard/dashboard.component';
import {ToastrModule} from 'ngx-toastr';
import {ButtonComponent} from './components/button/button.component';

import {registerLocaleData} from '@angular/common';
import localeEn from '@angular/common/locales/en';
import {UserComponent} from '@modules/main/header/user/user.component';
import {LanguageComponent} from '@modules/main/header/language/language.component';
import {PrivacyPolicyComponent} from './modules/privacy-policy/privacy-policy.component';
import {MainMenuComponent} from './pages/main-menu/main-menu.component';
import {SubMenuComponent} from './pages/main-menu/sub-menu/sub-menu.component';
import {MenuItemComponent} from './components/menu-item/menu-item.component';
import {DropdownComponent} from './components/dropdown/dropdown.component';
import {DropdownMenuComponent} from './components/dropdown/dropdown-menu/dropdown-menu.component';
import {ControlSidebarComponent} from './modules/main/control-sidebar/control-sidebar.component';
import {StoreModule} from '@ngrx/store';
import {authReducer} from './store/auth/reducer';
import {uiReducer} from './store/ui/reducer';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SelectComponent } from './components/select/select.component';
import { CheckboxComponent } from './components/checkbox/checkbox.component';
import { MarmitaReadComponent } from './pages/marmita/marmita-read/marmita-read.component';
import { MarmitaNewComponent } from './pages/marmita/marmita-new/marmita-new.component';
import { MarmitaUpdateComponent } from './pages/marmita/marmita-update/marmita-update.component';
import { TableComponent } from '@components/table/table.component';
import { MarmitaDeleteComponent } from './pages/marmita/marmita-delete/marmita-delete.component';
import { ResidenciaGeralNewComponent } from './pages/residenciaGeral/residencia-geral-new/residencia-geral-new.component';
import { ResidenciaGeralDeleteComponent } from './pages/residenciaGeral/residencia-geral-delete/residencia-geral-delete.component';
import { ResidenciaGeralUpdateComponent } from './pages/residenciaGeral/residencia-geral-update/residencia-geral-update.component';
import { ResidenciaGeralReadComponent } from './pages/residenciaGeral/residencia-geral-read/residencia-geral-read.component';

registerLocaleData(localeEn, 'en-EN');

@NgModule({
    declarations: [
        AppComponent,
        MainComponent,
        LoginComponent,
        HeaderComponent,
        FooterComponent,
        MenuSidebarComponent,
        RegisterComponent,
        DashboardComponent,
        ButtonComponent,
        UserComponent,
        LanguageComponent,
        PrivacyPolicyComponent,
        MainMenuComponent,
        SubMenuComponent,
        MenuItemComponent,
        DropdownComponent,
        DropdownMenuComponent,
        ControlSidebarComponent,
        SelectComponent,
        CheckboxComponent,
        MarmitaReadComponent,
        MarmitaNewComponent,
        MarmitaUpdateComponent,
        TableComponent,
        ForDirective,
        MarmitaDeleteComponent,
        ResidenciaGeralNewComponent,
        ResidenciaGeralDeleteComponent,
        ResidenciaGeralUpdateComponent,
        ResidenciaGeralReadComponent
    ],
    imports: [
        BrowserModule,
        StoreModule.forRoot({auth: authReducer, ui: uiReducer}),
        HttpClientModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot({
            timeOut: 3000,
            positionClass: 'toast-top-right',
            preventDuplicates: true
        }),
        NgbModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}
