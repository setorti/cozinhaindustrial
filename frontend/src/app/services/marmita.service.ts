import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { Marmita } from '../pages/marmita/marmita.model';

@Injectable({
  providedIn: 'root'
})
export class MarmitaService {
   // url do backend

  private baseUrl = "http://localhost:4040/marmita"

  constructor(
    private http: HttpClient) { }

    create(marmita: Marmita): Observable<Marmita>{
        return this.http.post<Marmita>(this.baseUrl, marmita)

    }

    read(): Observable<Marmita[]> {
        return this.http.get<Marmita[]>(this.baseUrl)
    }



    readById(id: any): Observable<Marmita>{
      const url= `${this.baseUrl}/${id}`
      return this.http.get<Marmita>(url)

    }

    update(marmita: Marmita): Observable<Marmita>{
      const url= `${this.baseUrl}/${marmita.id}`
      return this.http.put<Marmita>(url, marmita)

    }

    delete(id: any): Observable<Marmita>{
      const url= `${this.baseUrl}/${id}`
      return this.http.delete<Marmita>(url)
    }
  }




