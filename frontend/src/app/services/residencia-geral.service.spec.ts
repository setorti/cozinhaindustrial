import { TestBed } from '@angular/core/testing';

import { ResidenciaGeralService } from './residencia-geral.service';

describe('ResidenciaGeralService', () => {
  let service: ResidenciaGeralService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ResidenciaGeralService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
