import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Usuario } from '@modules/login/usuario.model';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

   // url do backend

   private baseUrl = "http://localhost:4041/loginResidencia"

   constructor(
     private http: HttpClient) { }

     create(usuario: Usuario): Observable<Usuario>{
         return this.http.post<Usuario>(this.baseUrl, usuario)

     }

     read(): Observable<Usuario[]> {
         return this.http.get<Usuario[]>(this.baseUrl)
     }



     readById(id: any): Observable<Usuario>{
       const url= `${this.baseUrl}/${id}`
       return this.http.get<Usuario>(url)

     }

     update(usuario: Usuario): Observable<Usuario>{
       const url= `${this.baseUrl}/${usuario.id}`
       return this.http.put<Usuario>(url, usuario)

     }

     delete(id: any): Observable<Usuario>{
       const url= `${this.baseUrl}/${id}`
       return this.http.delete<Usuario>(url)
     }
}
