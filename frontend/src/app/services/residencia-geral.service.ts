import { Injectable } from '@angular/core';
import { ResidenciaGeral } from '@pages/residenciaGeral/residenciaGeral.model';

import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ResidenciaGeralService {

   // url do backend

   private baseUrl = "http://localhost:4040/residenciaGeral"

   constructor(
     private http: HttpClient) { }

     create(residenciageral: ResidenciaGeral): Observable<ResidenciaGeral>{
         return this.http.post<ResidenciaGeral>(this.baseUrl, residenciageral)

     }

     read(): Observable<ResidenciaGeral[]> {
         return this.http.get<ResidenciaGeral[]>(this.baseUrl)
     }



     readById(id: any): Observable<ResidenciaGeral>{
       const url= `${this.baseUrl}/${id}`
       return this.http.get<ResidenciaGeral>(url)

     }

     update(residenciageral: ResidenciaGeral): Observable<ResidenciaGeral>{
       const url= `${this.baseUrl}/${residenciageral.id}`
       return this.http.put<ResidenciaGeral>(url, residenciageral)

     }

     delete(id: any): Observable<ResidenciaGeral>{
       const url= `${this.baseUrl}/${id}`
       return this.http.delete<ResidenciaGeral>(url)
     }
}
